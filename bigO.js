// Untuk O(1)
let firstelementarray = ['first', 'second', 'third', 'fourth', 'fifth'];
console.log((firstelementarray[0]));

const items = ['One', 'Two', 'Three', 'Four', 'Five'];

// untuk O(n²)
logAllPairs = (items) => {
  for (let i = 0; i < items.length; i++) {
    for (let j = 0; j < items.length; j++) {
      console.log(items[i], items[j]);
    }
  }
}

logAllPairs(items)